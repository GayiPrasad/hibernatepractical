package hibernate;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class RetrieveAccountDetails {
	public static void main(String args[]) throws JsonGenerationException,
			JsonMappingException, IOException {

		StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
				.configure("hibernate/hibernate.cfg.xml").build();
		Metadata metadata = new MetadataSources(standardRegistry)
				.getMetadataBuilder().build();
		SessionFactory sessionFactory = metadata.getSessionFactoryBuilder()
				.build();

		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Query<Account> query = session.createQuery("from Account");

		List<Account> accounts = query.getResultList();

		/*
		 ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true); 
		 
	 
		String json = mapper.writeValueAsString(accounts);
		 System.out.println("json " + json);*/
		 
		System.out.println("accounts" + accounts.toString());
		for (Account account : accounts) {
			System.out.println("AccountNumber " + account.getAccountNumber()
					+ ", Balance: " + account.getBalance() + "customers"
					+ account.getCustomers());
		}
	}
}
