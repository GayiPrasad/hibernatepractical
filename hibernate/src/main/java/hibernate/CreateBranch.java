package hibernate;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class CreateBranch {
	public static void main(String[] args) {
	
        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder() .configure("hibernate/hibernate.cfg.xml").build();
        
        Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
        
       SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
       
			Session session = sessionFactory.openSession();
			
			Transaction tx = session.beginTransaction();
			Branch branch = new Branch();
			 
		    branch.setId(3);
		    branch.setName("BASAVANNAGUDI");
		    branch.setCity("BANGALORE");
		    branch.setState("KARNATAKA");
		    branch.setDate(new Date());
			session.save(branch);
			
			System.out.println("Branch Has been created successfully !!");
			tx.commit();
			session.close();
			sessionFactory.close();
			
		}
}
