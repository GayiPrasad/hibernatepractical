package hibernate;

import java.util.List;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class ReadUser {
	
	public static void main(String args[]){
	/*Configuration cfg = new Configuration();
	cfg.configure("hibernate/hibernate.cfg.xml");
	 ServiceRegistry serviceRegistry =  new StandardServiceRegistryBuilder().applySettings(
		            cfg.getProperties()).build();
		    
		   SessionFactory sessionFactory = cfg.buildSessionFactory(serviceRegistry);*/ 
		StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder() .configure("hibernate/hibernate.cfg.xml").build();
	        Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
	       SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
		
		Session session = sessionFactory.openSession();
		 session.beginTransaction();
		 
		 Query<User> query = session.createQuery("from User");
		 
		List<User> users = query.getResultList();
		  for(User user :users)
		  {
		System.out.println("User Name "+user.getUserName()+", Password: "+ user.getPassword());
		  }
	}

}
