package hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class CreateCustomer {
	public static void main(String[] args) {
		
        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder() .configure("hibernate/hibernate.cfg.xml").build();
        
        Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
        
       SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
       
			Session session = sessionFactory.openSession();
			
			Transaction tx = session.beginTransaction();  
			Set<Address> listOfAddress = new HashSet();
		 
			
			Customer customer = new Customer();
			customer.setId(1); // Need to generate dynamically
		    customer.setDate(new Date());
		    
		    customer.setName("PRASAD");
		    // Address object
		    Address address = new Address();
		    address.setCity("BANGALORE");
		    address.setCountry("INDIA");
		    address.setCreatedDate(new Date());
		    
		    address.setState("KARNATAKA");
		    address.setStreet("XYZ");
		    address.setCustomer(new Customer());
		    address.getCustomer().setId(customer.getId());
		    // Added the Address object to list
		    listOfAddress.add(address);
		    // Added the list of Address to Customer
		    customer.setListOfAddress(listOfAddress);
			session.save(customer);
			
			System.out.println("Branch Has been created successfully !!");
			tx.commit();
			session.close();
			sessionFactory.close();
		}
}
