package hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class DeleteUser {

	public static void main(String args[]){
		
		/*Configuration cfg = new Configuration();
		cfg.configure("hibernate/hibernate.cfg.xml");
		 ServiceRegistry serviceRegistry =  new StandardServiceRegistryBuilder().applySettings(
			            cfg.getProperties()).build();
			    
			   SessionFactory sessionFactory = cfg.buildSessionFactory(serviceRegistry);*/
		 StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder() .configure("hibernate/hibernate.cfg.xml").build();
	        Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
	       SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
	       
			Session session = sessionFactory.openSession();
			
			 session.beginTransaction();
			 
			 User user = (User)session.load(User.class, 5);
			 session.delete(user);
			 
			 System.out.println("Deleted Successfully");
			  session.getTransaction().commit();
			     sessionFactory.close();
	}
}
