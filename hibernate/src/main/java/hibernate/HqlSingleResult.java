package hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

public class HqlSingleResult {
	public static void main(String args[]){
		/*Configuration cfg = new Configuration();
		cfg.configure("hibernate/hibernate.cfg.xml");
		 ServiceRegistry serviceRegistry =  new StandardServiceRegistryBuilder().applySettings(
			            cfg.getProperties()).build();
			    
			   SessionFactory sessionFactory = cfg.buildSessionFactory(serviceRegistry);*/ 
			StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder() .configure("hibernate/hibernate.cfg.xml").build();
		        Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
		       SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
			
			Session session = sessionFactory.openSession();
			 session.beginTransaction();
			 Query query = session.createQuery("from User u where u.userId=:userId");
			 query.setParameter("userId", 3);
			 
			User user = (User) query.getSingleResult();
			 
			System.out.println("User Name "+user.getUserName()+", Password: "+ user.getPassword());
			 
		}
}
