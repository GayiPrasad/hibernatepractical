package hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class RetrieveBranchDetails {
	public static void main(String args[]){
	StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder() .configure("hibernate/hibernate.cfg.xml").build();
    Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
   SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
   
Session session = sessionFactory.openSession();

 session.beginTransaction();
 
 Branch branch = (Branch)session.load(Branch.class, 3);
 
System.out.println("branch DEtails " + branch.getId() +" " + branch.getName() + " " + branch.getCity());



System.out.println("Account Details  " + branch.getListOfAccounts());

  System.out.println("Updated Successfully");
  
  session.getTransaction().commit();
  sessionFactory.close();
}

}
