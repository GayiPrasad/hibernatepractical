package hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

public class HqlUpdateUser {
	@SuppressWarnings("rawtypes")
	
	public static void main(String args[]){
	  StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder() .configure("hibernate/hibernate.cfg.xml").build();
      Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
     SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
			Session session = sessionFactory.openSession();
			
			String updateQuery ="UPDATE User u SET u.password=:password where u.userId=:userId";
					
		
			Transaction tx = session.beginTransaction();
		
		Query query	= session.createQuery(updateQuery);
		query.setParameter("password", "newPassword");
		query.setParameter("userId", 8);
			
			/*String updateQuery ="UPDATE User u SET u.password= ? where u.userId=?";
			Transaction tx = session.beginTransaction();
		
		Query query	= session.createQuery(updateQuery);
		query.setParameter(0, "newPassword1");
		query.setParameter(1, 8);
		*/
		
		int update = query.executeUpdate();
			System.out.println("User has saved successfully !!" + update);
			tx.commit();
			session.close();
			sessionFactory.close();
		}
}
