package hibernate;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class AccountCustomerCreation {

	public static void main(String[] args) {

		StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
				.configure("hibernate/hibernate.cfg.xml").build();

		Metadata metadata = new MetadataSources(standardRegistry)
				.getMetadataBuilder().build();

		SessionFactory sessionFactory = metadata.getSessionFactoryBuilder()
				.build();

		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();
		Set<Address> listOfAddress = new HashSet();
		Set<Customer> listOfCustomers = new HashSet();

		Account account = new Account();
		account.setAccountNumber("12345678901112345"); // Neeed to generate 16 digits
										// dynamically
		account.setBalance(0L);
		account.setBranch(new Branch());
		account.getBranch().setId(3);
		account.setCreatedDate(new Date());
		account.setModifiedDate(new Date());
		account.setType(new AccountType());
		account.getType().setId(1);

		//  First Customer Details
		Customer firstCustomer = new Customer();
		firstCustomer.setId(1); // Need to generate dynamically
		firstCustomer.setDate(new Date());
		firstCustomer.setName("GAYI");
		// Second Customer Details
		Customer secondCustomer = new Customer();
		secondCustomer.setId(2); // Need to generate dynamically
		secondCustomer.setDate(new Date());
		secondCustomer.setName("GAYITHRI");
		// First Address object
		Address address = new Address();
		address.setCity("BANGALORE");
		address.setCountry("INDIA");
		address.setCreatedDate(new Date());
		
		address.setState("KARNATAKA");
		address.setStreet("XYZ");
		address.setCustomer(new Customer());

		address.getCustomer().setId(secondCustomer.getId());

		// Second Address object
		Address address1 = new Address();
		address1.setCity("BANGALORE");
		address1.setCountry("INDIA");
		address1.setCreatedDate(new Date());
		
		address1.setState("KARNATAKA");
		address1.setStreet("XYZ");
		address1.setCustomer(new Customer());

		address1.getCustomer().setId(secondCustomer.getId());

		// Added the first, second Address object to list
		listOfAddress.add(address);
		listOfAddress.add(address1);
		//customer.setListOfAddress(listOfAddress);

		listOfCustomers.add(firstCustomer);
		listOfCustomers.add(secondCustomer);
		
		account.setCustomers(listOfCustomers);
		session.save(account);

		System.out.println("Branch Has been created successfully !!");
		tx.commit();
		session.close();
		sessionFactory.close();
	}

}
